package mobi.pinuts.prova.android.models;

import java.util.ArrayList;

import mobi.pinuts.prova.android.R;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;

public class BookmarkCollection extends ArrayList<Bookmark> {

	private static final long serialVersionUID = -8837310291989789567L;
	
	public static BookmarkCollection getTestBookmarks(Context context) {
		BookmarkCollection bookmarks = new BookmarkCollection();
		
		String[] titles  = context.getResources().getStringArray(R.array.bookmark_titles);
		String[] urls    = context.getResources().getStringArray(R.array.bookmark_urls);
		TypedArray icons = context.getResources().obtainTypedArray(R.array.bookmark_icons);
		
		for (int i = 0; i < titles.length; i ++) {
			bookmarks.add(titles[i], urls[i], icons.getDrawable(i));
		}
		
		return bookmarks;
	}

	public void add(String title, String url, Drawable icon) {
		Bookmark newBookmark = new Bookmark(title, url, icon);
		add(newBookmark);
	}
	
}
