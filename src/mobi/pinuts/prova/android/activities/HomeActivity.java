package mobi.pinuts.prova.android.activities;

import mobi.pinuts.prova.android.R;
import android.app.Activity;
import android.os.Bundle;

public class HomeActivity extends Activity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.home_main);
    }
}
